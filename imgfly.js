/**
 * @file
 * Javascript file that performs the image path manipulations.
 */
(function ($) {

  // Attaching to Drupal behaviors
  window.imgflySrc = window.imgflySrc || "data-src";
  window.imgflyAspect = window.imgflyAspect || "data-aspect";
  Drupal.behaviors.imgfly_js = {

    // Make these variable accessible also outside this module
    loadwatch: function() {
      return 0 + $(window).scrollTop() + $(window).height() + parseInt(Drupal.settings.imgfly.loadvalue);
    },

    imagereplace: function(inline, imgEl){
      //Check for inline overrides.
      if (inline) {
        var newPath = Drupal.behaviors.imgfly_js.imagepath($('#' + imgEl));
        if ($('#' + imgEl).attr('src') != newPath) {
          $('#' + imgEl).attr('src', newPath);
        }
      }
      else {
        $('.imgfly-placeholder').each(function() {
          var newPath = Drupal.behaviors.imgfly_js.imagepath($(this));
          if ($(this).attr('src') != newPath) {
            if (Drupal.settings.imgfly.lazyload){
              if ($(this).offset().top < Drupal.behaviors.imgfly_js.loadwatch()) {
                $(this).attr('src', newPath);
              }
            }
            else {
              $(this).attr('src', newPath);
            }
          }
        });
      }
    },

    imagepath: function(thisEl){
      var imgSrc = thisEl.attr(window.imgflySrc);
      var aspect = thisEl.attr(window.imgflyAspect);
      var newPath = '';
      if (imgSrc == undefined && window.imgflySrc != 'data-src') {
        imgSrc = thisEl.attr('data-src');
        aspect = thisEl.attr('data-aspect');
      }

      if (imgSrc){
        var imgWidth = thisEl.width();
        var imgHeight = (aspect > 0) ? Math.round(imgWidth / aspect) : thisEl.height();
        var imgflyPath = 'imgfly/' + imgWidth + '/' + imgHeight;
        newPath = imgSrc.replace(/sites\/\S+\/files/, imgflyPath);
      }
      return newPath;
    },

    attach: function (context, settings) {
      Drupal.behaviors.imgfly_js.imagereplace();
    }
  };
}(jQuery));
